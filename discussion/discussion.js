// CRUD Operations:
/*
    1. Create - insert
    2. Read - find
	3. Update - update
	4. Destroy - delete
*/

//Insert Method () - create documents in our DB
/*
    syntax:
        insert one document:
            db.collectionName.insertOne({
                "fieldA": "valueA",
                "fieldB": "valueB",
            });

            insert many documents:
                db.collectionName.insertMany([
                    {
                        "fieldA": "valueA",
                        "fieldB": "valueB",
                    },
                    {
                        "fieldA": "valueA",
                        "fieldB": "valueB",
                    }
                ]);
*/

//TODO: upon execution on robo 3t make sure not to execute your data several times as it will duplicate.

db.users.insertOne({
    "firstName": "Jane",
    "lastName": "Dela Cruz",
    "age": 21,
    "email": "jdoe@example.com",
    "company": "none"
})

db.users.insertMany([
    {
        "firstName": "Stephen",
        "lastName": "Hawkings",
        "age": 67,
        "email": "stephen@example.com",
        "department": "none"
    },
    {
        "firstName": "Niel",
        "lastName": "Armstrong",
        "age": 82,
        "email": "nielArmstrong@example.com",
        "department": "none"
    }
])

//Mini Activity
	/*
		1. Make a new collection with the name "courses"
		2. Insert the following fields and values

			name: Javascript 101
			price: 5000
			description: Introduction to Javascript
			isActive: true

			name: HTML 101
			price: 2000
			description: Introduction to HTML
			isActive: true

			name: CSS 101
			price: 2500
			description: Introduction to CSS
			isActive: true
*/
db.courses.insertMany([
    {
        "name": "Javascript 101",
        "price": 5000,
        "description": "Introduction to Javascript",
        "isActive": true
    },
    {
        "name": "HTML 101",
        "price": 2000,
        "description": "Introduction to HTML",
        "isActive": true
    },
    {
        "name": "CSS 101",
        "price": 2500,
        "description": "Introduction to CSS",
        "isActive": true
    }
])

// read - find method
/*
    read or find data from the database

    syntax:
        db.collectionName.find() - this will retrieve all the documents from our database

        db.collectionName.find({"criteria": "value"}) - this will retrieve all the documents that will match our criteria

        db.collectionName.findOne({"criteria": "value"}) - this will return the first document in our collection that match our criteria

        db.collectionName.findOne({}) - this will return the first document in our collection
*/

db.users.find();

db.users.find({
    "firstName": "Jane"
});

db.users.find({
    "firstName": "Niel",
    "age": 82
});

/*
    Update Documents method - updates our documents in our collection

    updateOne() - updating the first mathcing document on our collection
    Syntax:
        db.collectionName.updateOne({
            "criteria": "value"
        },
        {
            $set: {
                "fieldToBeUpdated": "Updated Value"
            }
        });

        updateMany() - multiple; it updates all the documents that matches our criteria

    Syntax:
        db.collectionName.updateMany({
            "criteria": "value"
        },
        {
            $set: {
                "fieldToBeUpdated": "Updated Value"
            }
        })
*/

db.users.insertOne({
    "firstName": "Test",
    "lastName": "Test",
    "age": 0,
    "email": "test@example.com",
    "department": "none"
});

//updating one Document

db.users.updateOne(
    {
    "firstName": "Test"
    },
    {
    $set: {
        "firstName": "Bill",
        "lastName": "Gates",
        "age": 65,
        "email": "bill@example.com",
        "department": "Operations",
        "status": "active"
        }
    }
);

//Removing a field
db.users.updateOne(
    {
        "firstName": "Bill"
    },
    {
        $unset: {
            "status": "active"
        }
    }
)

// updating multiple Document

db.users.updateMany(
    {
        "department": "none"
    },
    {
        $set: {
            "department": "HR",
        }
    }
)

db.users.updateOne({},
    {
        $set: {
            "department": "operations",
        }
    })

//mini activity

db.courses.updateOne(
    {
        "name": "HTML 101"
    },
    {
        $set: {
            "isActive": false
        }
    }
)

db.courses.updateMany({},
    {
        $set: {
            "enrollees": 10
        }
    })

// destroy/delete documents method - deleting documents from our collection

db.users.insertOne({
    "firstName": "Test"
})

//deleting a single document
/*  
    Syntax:
        db.collectionName.deleteOne({"criteria": "value"})
*/
db.users.deleteOne({
    "firstName": "Test"
})

//deleting multiple documents
/* 
    Syntax:
        db.collectionName.deleteMany({"criteria": "value"})
*/

db.users.deleteMany({
    "department": "HR"
})

//delete all content
db.courses.deleteMany({})
